<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
      return view('register');
    }

    public function welcome(Request $req){

      $first_name = $req->input('first_name');
      $last_name = $req->input('last_name');

      return view('welcome', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
