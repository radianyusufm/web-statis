<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>belajar html 1</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
      @csrf

      <label>First Name</label>
      <br>
      <input type="text" name="first_name" >
      <br>
      <label>Last Name</label>
      <br>
      <input type="text" name="last_name">
      <br>


      <p>Gender :</p>
      <input type="radio" name="gender " value="male">
      <label>Male</label>
      <br>
      <input type="radio" name="gender" value="female">
      <label>Female</label>
      <br>
      <input type="radio" name="gender" value="other">
      <label>Other</label>
      <br>

      <p>Nationality :</p>
      <select>
        <option>Indonesia</option>
        <option>Singapura</option>
        <option>Malaysia</option>
      </select>

      <p>Language Spoken :</p>
      <input type="checkbox" >
      <label>Bahasa Indonesia</label>
      <br>
      <input type="checkbox">
      <label>English</label>
      <br>
      <input type="checkbox">
      <label>Other</label>
      <br>

      <p>Bio :</p>
      <textarea rows="8" cols="40"></textarea>

      <br>
      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
